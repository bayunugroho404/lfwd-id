/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class Movie {
  final String title;
  final String genre;
  final String year;
  final String imageUrl;

  Movie({this.genre, this.title, this.year, this.imageUrl});
}