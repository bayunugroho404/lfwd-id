import 'package:flutter/material.dart';
import 'package:flutter_widget/ui/view/column_page.dart';
import 'package:flutter_widget/ui/view/form_page.dart';
import 'package:flutter_widget/ui/view/listview_page.dart';
import 'package:flutter_widget/ui/view/row_page.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => ColumnPage()));
                },
                child: Text('Column'),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => RowPage()));
                  },
                child: Text('Row'),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => FormPage()));
                },
                child: Text('Form Page'),
              ),
              RaisedButton(
                onPressed: () {
                  _onAlertWithCustomImagePressed(context);
                },
                child: Text('Alert Dialog'),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListViewPage()));
                },
                child: Text('ListView dan Floating Button '),
              )
            ],
          ),
        ),
      ),
    );
  }
  _onAlertWithCustomImagePressed(context) {
    Alert(
      context: context,
      title: "RFLUTTER ALERT",
      desc: "Flutter is more awesome with RFlutter Alert.",
    ).show();
  }
}
