import 'package:flutter/material.dart';
import 'package:flutter_widget/ui/widget/column_and_row_widget.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class ColumnPage extends StatefulWidget {
  @override
  _ColumnPageState createState() => _ColumnPageState();
}

class _ColumnPageState extends State<ColumnPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Column Example'),),
      body: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              itemColumn(Colors.red),
              itemColumn(Colors.blue),
              itemColumn(Colors.green),
              itemColumn(Colors.amber),
              itemColumn(Colors.black)
            ],
          ),
        ),
      ),
    );
  }


}
