import 'package:flutter/material.dart';
import 'package:flutter_widget/ui/widget/column_and_row_widget.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

class RowPage extends StatefulWidget {
  @override
  _RowPageState createState() => _RowPageState();
}

class _RowPageState extends State<RowPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Row Example'),),
      body: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              itemColumn(Colors.red),
              itemColumn(Colors.blue),
              itemColumn(Colors.green),
              itemColumn(Colors.amber),
              itemColumn(Colors.black)
            ],
          ),
        ),
      ),
    );
  }


}
