import 'package:flutter/material.dart';

/**
 * Created by Bayu Nugroho
 * Copyright (c) 2020 . All rights reserved.
 */

Widget itemColumn(Color warna) {
  return Container(
    margin: EdgeInsets.all(8.0),
    color: warna,
    width: 100,
    height: 100,
  );
}